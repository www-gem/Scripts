# My scripts in self-service

## dmenu: a menu to launch them all
This script uses dmenu to automatically run a command based on the text thrown at it.

Features:

- execute a command if the input text belongs to /bin or /usr/local/bin
- open url in Brave if the input text starts with http
- translate the input text in English/French using the deepl website if the input text starts with dpen/dpfr
- search the input text with duduckgo (if all the above failed)
- this script also reconnects my vpn to a random US location if no instance of Brave nor vpnc are running

Requirements:

- [dmenu](https://tools.suckless.org/dmenu/)
- [menu-calc](https://github.com/sumnerevans/menu-calc) if you want to do simple calculation. menu-calc uses bc as the backend and will accept any operations bc is able to do
- [xclip](https://github.com/astrand/xclip) if you want to use copy/paste in dmenu
- a web browser and your preferred search engine. Here I'm using [Brave](https://brave.com/) and [duckduckgo](https://duckduckgo.com/)
- part of this script also handle my vpn connection automatically [expressvpn](https://www.expressvpn.com)


## dmenu-copyq: manage your clipboard with dmenu
This script aims at showing the last 99 items stored in copyq history and to allow selection of the desired one. The selection will automatically be pasted in the active window.
There is also an option to delete entries from the history.

Requirements:

- [dmenu](https://tools.suckless.org/dmenu/)
- [copyq](https://github.com/hluk/CopyQ)


## dunst_volume: use dunst as a volume indicator
This script uses dunst to display your volume level.

Features:

- show a volume level notification (with a progress bar)
- execute pulseeffects on mouse click on the notification

Requirements:

- [dunst](https://github.com/dunst-project/dunst)
- [ponymix](https://github.com/falconindy/ponymix) to show the volume info
- (optional) [pulseeffects](https://github.com/wwmm/pulseeffects)

## infobar
This script can be used to populate your windows manager infobar with some information. It can be used with any bar that supports reading info from bash.

Features:

- show date/time
    - time in 12H format
    -  date: abbreviated weekday, abbreviated month, day of month
- show free disk space
    - free space on / (in Go)
    - free space on /home (in Go)
- show free memory (in %)
- show wifi network
    - signal strength (in %)
    - access point name
- show vpn status (disconnected or location) Require expressvpn
- show battery info
    - level (in %)
    - remaining time or time to full charge
    - status ("+" for charging)
- show the volume (in % or "mute")
- show a systray (define your own icons for your desired applications)

Requirements:

- (optional) expressvpn to show vpn info. You can use any vpn client but you will have to adjust the commands used inside the Wifi network section of the script
- [ponymix](https://github.com/falconindy/ponymix) to show the volume info
- a font that works with bash for systray information

Some screenshots:

vpn on | volume off
- ![](vpn_on-mute.png)

vpn on | volume off | 1 app in systray (symbolized with the check mark icon)
- ![](vpn_on-mute-tray.png)

vpn on | volume on | 1 app in systray (symbolized with the check mark icon)
- ![](vpn_on-volume-tray.png)

 vpn disconnected | volume on | 1 app in systray (symbolized with the check mark icon)
- ![](vpn_off-volume-tray.png)


## khal_reminder: add reminders to khal
This script is using khal and vdirsyncer to update your calendar and notifies you about any upcoming event occuring in the next hour.

Requirements:

- [khal](https://github.com/pimutils/khal)
- [vdirsyncer](https://github.com/pimutils/vdirsyncer)
- crontab
- (optional) [dunst](https://github.com/dunst-project/dunst)

To run the script periodically, add a crontab rule.

## rofi-pass.sh

This script is to be used with dmenu or rofi. It will list all websites for which you have a password managed by pass and will call the totp token for the selected website thanks to totp-cli if it exists.
The script will append each entry with a number to make selection easier/faster than writing the full website's name when calling the options listed in features below.

There is a dmenu wrapper called passmenu but it is limited to copy your password to the clipboard. I came up with this script to add the options to copy your username, password, or totp token and to automatically paste it in the active window when selected. It can also take care of the entire login process for you if you prefer. By pressing Enter, it will automatically fill the username and password fields and show you the totp token if it exists and wait for you to select it when the website is ready to accept it.

Note that the script manages the 2 most common username/password fields formatting options: 1) the username and password fields are on two different pages, 2) the 2 fields are on the same page. By default, the script expects to see the username and password fields on the same page. If a website is offering the other formatting option, you can tell the script to act accordingly by adding "Tab" on line 4 of your password file.

Features:

(where XX is the number in front of the record)
- lXX: show url and automatically paste in active window when/if selected
- pXX: show password and automatically paste in active window when/if selected
- tXX: show totp token (if exists) and automatically paste in active window when/if selected
- uXX: show username and automatically paste in active window when/if selected
- enter: automatically paste username and password in active window. Will prompt totp token and paste it when/if selected

Following one of this command with a number will execute the command for the given website (e.g. u12 will show you the username for the website#12).

More info can be found in the rofi-pass-README file on this repository.

Requirements:

- in its current state, the script is looking for info stored in ~/.password-store/Pwd but you can change that to fit specific needs.
- [dmenu](https://tools.suckless.org/dmenu/) or [rofi](https://github.com/davatorium/rofi) you can use either one of them. This script is written to use rofi but you can easily adapt it to use dmenu if you replace the rofi commands by dmenu.
- [pass](https://www.passwordstore.org/)
- [gpg]
- [xdotool] to simulate keypress
- (optional) [totp-cli](https://github.com/WhyNotHugo/totp-cli) if you want to manage/access your totp tokens. totp-cli is a simple command line application to generate OTP tokens for two factor authentication using RFC6238. totp-cli fetches your shared key from pass
- (optional) [copyq] is used here as my clipboard manager for several reasons (see my website of gemini capsule for details) but you can use xclip or something similar if you prefer
