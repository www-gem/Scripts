#!/usr/bin/env bash
while true; do
	# Date/Time (time in 12H, Mon Jan 1)
	date=$(date | awk '{print $1, $2, $3}')
	time=$(date +%I:%M)

	# Free Space (in Go)
	free="$(df -h / | awk 'NR==2 {print $4}') -" #free space on /
	freehome="$(df -h /home | awk 'NR==2 {print $4}')" #free space on/home

	# Free memory (in %)
	mfree=$(free -mt | awk 'NR==4 {print $4}') #free memory in Mo
	mtot=$(free -mt | awk 'NR==4 {print $2}') #total memory
	mem="$(echo "($mfree/$mtot)*100" | bc -l | sed 's/\..*//')%" #free memory in percentage

	# Wifi Network (signal strength in %, access point name, vpn status/location)
	if iwgetid -a | grep -q 'wlp0s20f3' ; then #replace wlp0s20f3 with your interface (can be found with ifconfig)
		if iwgetid -a | grep -q 'A7:C5' ; then #replace A7:C5 with unique bit of your access point MAC address (can be found with iwgetid -a)
			wla="TP-Link"
		elif iwgetid -a | grep -q '40:B0' ; then #same as above for a second access point
			wla="Asus"
		else
			wla="" #define $wla as empty if access point is unknown
		fi
		if expressvpn status | grep -q 'Not connected' ; then
			vpn="VPN DISCONNECTED"

			#define $vpn as the vpn location (with a tweak if expressvpn advertises for a new available version
		elif [ "$(expressvpn status | awk NR==1 | awk '{print $5, $6}')" == "available, download" ] ; then
			vpn=$(expressvpn status | awk NR==3 | awk '{print $5, $6}')
		else
			vpn=$(expressvpn status | awk NR==1 | awk '{print $5, $6}')
		fi
		wln="$( < /proc/net/wireless awk 'NR==3 {printf($3*10/7)}' | sed 's/\..*//')% $wla ($vpn)"
		if ! iwgetid -a | grep -q 'wlp0s20f3' ; then
			wln="No Network"
			wla=""
		fi
	fi

	# Battery (level in %, remaining time/time to full charge, + for charging/LOW if level <20)
	batst=$(acpi | awk 'NR==1 {print $3}' | sed 's/,//')
	#hidden if battery full
	if [ "$batst" == "Full" -o "$batst" == "Unknown" ]; then
		bat=""
		#format battery charging
	elif [ "$batst" == "Charging" ]; then
		perc="$(acpi | awk 'NR==1 {print $4}' | sed 's/,//')"
		charg="$(acpi | awk 'NR==1 {print $5}' | sed 's/.\{3\}$//')"
		bat="$perc ($charg) +"
		#format battery discharging
	elif [ "$batst" == "Discharging" ] && [ $(acpi | awk 'NR==1 {print $4}' | sed 's/,//' | sed 's/%//') -lt "20" ]; then
		perc="$(acpi | awk 'NR==1 {print $4}' | sed 's/,//')"
		charg="$(acpi | awk 'NR==1 {print $5}' | sed 's/.\{3\}$//')"
		bat="$perc ($charg) LOW"
	else
		perc="$(acpi | awk 'NR==1 {print $4}' | sed 's/,//')"
		charg="$(acpi | awk 'NR==1 {print $5}' | sed 's/.\{3\}$//')"
		bat="$perc ($charg)"
	fi

	# Volume (level in %/mute)
	if [ "$(amixer sget Master,0 | awk 'NR==5 {print $5}')" == "[on]" ]; then
		vol="$(ponymix get-volume)%"
	else
		vol="mute"
	fi

	#systray
	# Davmail
	if [ -n "$(pidof java)" ]; then
		davmail="✓"
	else
		davmail=""
	fi

	# Spaces
	spaces=$(printf "%5s")

	xsetroot -name "$(echo -e "$time" "$spaces" "$date" "$spaces" "$free" "$freehome" "$spaces" "$mem" "$spaces" "$wln" "$spaces" "$bat" "$spaces" "$vol" "$spaces" "$davmail" "$nextc" "$keepxc")" #command to integrate the info in dwm with no bar patches (to be adjusted if you're using something else than dwm as your windows manager)

	sleep 2 #update interval in seconds
done
