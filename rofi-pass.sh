#!/usr/bin/env bash
clip () {
    copyq add "$1"
    copyq -- select 0
    sleep 0.2 && copyq paste
    copyq remove 0
}

site=()
sel=()
num=0
mapfile -t items < <(ls ~/.password-store/Pwd | sed 's/.gpg//')
for i in "${items[@]}"
do
    site+=("$i")
    sel+=("$num"": ""$i")
    let "num+=1"
done

action=$(printf '%s\n' "${sel[@]}" | rofi -dmenu -p "" -i -l 5)
sel=${site[$(echo "$action" | sed 's/^.//')]}
case $action in
    "") ;;
    # RETRIEVE URL
    "l"*) link=$(pass show Pwd/"$sel" | awk 'NR==3' | rofi -dmenu -i -l 1)
	   clip "$link" ;;
    # RETRIEVE PASSWORD
    "p"*) pass=$(pass show Pwd/"$sel" | awk 'NR==1' | rofi -dmenu -i -l 1)
	   clip "$pass" ;;
    # RETRIEVE TOKEN
    "t"*) token=$(totp -n "$sel" | rofi -dmenu -i -l 1)
	   clip "$token" ;;
    # RETRIEVE USER
    "u"*) user=$(pass show Pwd/"$sel" | awk 'NR==2' | rofi -dmenu -i -l 1)
	   clip "$user" ;;
    # AUTO LOGIN
    *) sel=$(echo "$action" | awk '{print $2}')
       clip "$(pass show Pwd/"$sel" | awk 'NR==2')"
       if [ "$(pass show Pwd/$sel | awk 'NR==4')" == "Tab" ] ; then
	   xdotool key "Tab"
       else
	   xdotool key "Return"
           sleep 2
       fi
       clip "$(pass show Pwd/"$sel" | awk 'NR==1')"
       xdotool key "Return"
       if ls ~/.password-store/2fa | grep -q "^$sel$" ; then
	  token=$(totp -n "$sel" | rofi -dmenu -i -l 1)
	  clip "$token"
	fi ;;
esac
