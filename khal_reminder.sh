#!/usr/bin/env bash
#Description: Calendar reminder notifications for all events in the next hour

# needed to call notify-send from crontab
wm=$(wmctrl -m | head -1 | awk '{print $2}')
eval "export $(grep -E -z DBUS_SESSION_BUS_ADDRESS /proc/$(pgrep -u $LOGNAME $wm)/environ)"

vdirsyncer sync &
if [[ ! -z $(khal -c ~/.config/khal/config_reminder list now 1h) ]]; then
    khal -c ~/.config/khal/config_reminder list now 1h | tail -n +2 | awk NR==1 | sed 's/::\ _____.*//' | while read -r line ; do
    dunstify -u low --action="default,Reply" "Next event: $line"
fi