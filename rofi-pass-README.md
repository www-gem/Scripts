## Configure your gpg key to encrypt your passwords
Type gpg --full-gen-key in a terminal and leave everything by default unless you have specific requirements. When done, it will ask for your passphrase. You have to remember it and be sure to NEVER EVER forget it. This is your key and only mean to access the passwords/tokens you will create with pass/totp-cli.

## Initiate your pass
Type the following in a terminal:
> pass init  
> pass init gpg-id # use the gpg-id you've created above  
> pass insert -m Pwd/NAME # use whatever path you'd like and adjust the script if you're not going with Pwd.  
                          #For NAME go with whatever seems relevant to the website.

This last command will allow you to generate a file where each line will store one information. The script is written to work with the following file structure:
- line 1: your password.
- line 2: your username.
- line 3: the url of the website.
- line 4: write "Tab" (without the quotes) if you want the script to automatically fill the username/password fields of a website which is asking for your username on the first page and for your password on a second page.

Note that you can use the command "pass edit Pwd/NAME" to modify an existing file, and "pass generate NAME LENGTH" to generate a new password of LENGTHS characters for a file named NAME.
Also, you can copy/paste your passwords files and gpg key on your phone and use "password store"  to access/manage your passwords in Android.
This is why line 1 of the file structure here is dedicated to the password. Indeed, password store is expecting for the password on this line,.

## using totp-cli
In the settings of a website, find the 2 factors authentication settings. Once you have a QR code, look for the option to use a key instead and copy it when totp-cli is asking for a shared key after typing "totp -a NAME" in a terminal (where NAME is a relevant name for your file).
You can then manually generate a token by typing "totp -show NAME" in a terminal.

Note that if you're using an app like andotp on your Android phone to manage your tokens I recommend flashing the QR code and then creating a token with totp-cli so totp-cli and your phone app will use the same tokens to access the same website. If you do so, just double check that the generated token on the first use is the same on your computer and on your phone since I've noticed that sometimes it can differ (which is weird).
